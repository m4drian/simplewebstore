﻿using Microsoft.EntityFrameworkCore;
using SimpleWebStore.Models;

namespace SimpleWebStore.Data
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options) : base(options)
        {
        }

        public DbSet<PriceSuggestion> PriceSuggestions { get; set; }

        public DbSet<Product> Products { get; set; }

        public DbSet<User> Users { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // Relationships, constraints, etc.

            // PriceSuggestion
            modelBuilder.Entity<PriceSuggestion>()
                .HasKey(ps => ps.Id);

            modelBuilder.Entity<PriceSuggestion>()
                .Property(p => p.NewPrice)
                .HasPrecision(19, 4);

            modelBuilder.Entity<PriceSuggestion>()
                .Property(ps => ps.Status)
                .IsRequired()
                .HasConversion<string>(); // Store enum as string in the database

            // User
            modelBuilder.Entity<User>()
                .HasKey(u => u.Id);

            modelBuilder.Entity<User>()
                .Property(u => u.Role)
                .IsRequired()
                .HasConversion<string>();

            // Product
            modelBuilder.Entity<Product>()
                .HasKey(p => p.Id);

            modelBuilder.Entity<Product>()
                .Property(p => p.Price)
                .HasPrecision(19, 4);

            // Relationships, one-to-many
            modelBuilder.Entity<PriceSuggestion>()
                .HasOne(ps => ps.User)
                .WithMany(u => u.PriceSuggestions)
                .HasForeignKey(ps => ps.UserId);

            modelBuilder.Entity<PriceSuggestion>()
                .HasOne(ps => ps.Product)
                .WithMany(p => p.PriceSuggestions)
                .HasForeignKey(ps => ps.ProductId);
        }
    }
}
