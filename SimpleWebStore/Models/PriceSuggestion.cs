﻿using SimpleWebStore.Enums;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SimpleWebStore.Models
{
    public class PriceSuggestion
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        public decimal NewPrice { get; set; }

        [Required]
        [EnumDataType(typeof(PriceSuggestionStatus))]
        public PriceSuggestionStatus Status { get; set; }

        [Required]
        public int UserId { get; set; }

        [Required]
        public int ProductId { get; set; }

        //In this case it will always be a customer
        [Required]
        public User User { get; set; }

        [Required]
        public Product Product { get; set; }
    }
}
