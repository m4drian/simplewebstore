﻿#nullable enable
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics.CodeAnalysis;

namespace SimpleWebStore.Models
{
    public class Product
    {
        public Product()
        {
            Name = "";
            Category = "";
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        [AllowNull]
        [Required(AllowEmptyStrings = true)]
        public string? Description { get; set; }

        [Required]
        public string Category { get; set; }

        [Required]
        public decimal Price { get; set; }

        [AllowNull]
        public ICollection<PriceSuggestion>? PriceSuggestions { get; set; }
    }
}
