﻿#nullable enable
using SimpleWebStore.Enums;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics.CodeAnalysis;

namespace SimpleWebStore.Models
{
    public class User
    {

        public User() {
            Name = "";
            Login = "";
            PasswordHash = "";
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        [EnumDataType(typeof(UserRole))]
        public UserRole Role { get; set; }

        [Required]
        public string Login { get; set; }

        [Required]
        public string PasswordHash { get; set; }

        [AllowNull]
        public ICollection<PriceSuggestion>? PriceSuggestions { get; set; }
    }
}
