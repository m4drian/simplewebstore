﻿namespace SimpleWebStore.DTO
{
    public class PriceSuggestionDTO
    {
        public decimal NewPrice { get; set; }

        public int UserId { get; set; }

        public int ProductId { get; set; }
    }
}
