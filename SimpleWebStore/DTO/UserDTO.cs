﻿using SimpleWebStore.Enums;

namespace SimpleWebStore.DTO
{
    public class UserDTO
    {
        public string Name { get; set; }

        public UserRole Role { get; set; }

        public string Login { get; set; }

        public string PasswordHash { get; set; }
    }
}
