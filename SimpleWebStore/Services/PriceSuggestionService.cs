﻿using AutoMapper;
using SimpleWebStore.DTO;
using SimpleWebStore.Enums;
using SimpleWebStore.Models;
using SimpleWebStore.Repositories.Interfaces;
using SimpleWebStore.Services.Interfaces;

namespace SimpleWebStore.Services
{
    public class PriceSuggestionService : IPriceSuggestionService
    {

        private readonly IPriceSuggestionRepository _priceSuggestionRepository;
        private readonly IProductRepository _productRepository;
        private readonly IMapper _mapper;

        public PriceSuggestionService(
            IPriceSuggestionRepository priceSuggestionRepository,
            IProductRepository productRepository,
            IMapper mapper)
        {
            _priceSuggestionRepository = priceSuggestionRepository;
            _productRepository = productRepository;
            _mapper = mapper;
        }

        public async Task<PriceSuggestionDTO[]> GetPriceSuggestions()
        {
            var priceSuggestions = await _priceSuggestionRepository.GetPriceSuggestions();
            return _mapper.Map<PriceSuggestionDTO[]>(priceSuggestions);
        }

        public async Task<PriceSuggestionDTO[]> GetPriceSuggestionsByUserId(int userId)
        {
            var priceSuggestions = await _priceSuggestionRepository.GetPriceSuggestionsByUserId(userId);
            var mappedSuggestions = _mapper.Map<PriceSuggestionDTO[]>(priceSuggestions);
            return mappedSuggestions;
        }

        public async Task<bool> CreatePriceSuggestion(PriceSuggestionDTO priceSuggestion)
        {
            var existingSuggestionsCount = await _priceSuggestionRepository
                .GetAmountOfPriceSuggestionsByProductIdAndUserId(priceSuggestion.ProductId, priceSuggestion.UserId);

            if (existingSuggestionsCount >= 3)
            {
                return false;
            }

            var originalProduct = await _productRepository.GetProductById(priceSuggestion.ProductId);

            if (originalProduct == null)
            {
                return false;
            }

            if (priceSuggestion.NewPrice > originalProduct.Price * 2)
            {
                return false;
            }

            var mappedPriceSuggestion = _mapper.Map<PriceSuggestion>(priceSuggestion);
            mappedPriceSuggestion.Status = PriceSuggestionStatus.Pending;

            var success = await _priceSuggestionRepository.CreatePriceSuggestion(mappedPriceSuggestion);

            if (!success)
            {
                return false;
            }

            return true;
        }

        public async Task<bool> UpdatePriceSuggestionStatus(int updatedSuggestionId, int updatedSuggestionStatus)
        {
            var existingSuggestion = await _priceSuggestionRepository.GetPriceSuggestionById(updatedSuggestionId);

            if (existingSuggestion == null)
            {
                return false;
            }

            if (existingSuggestion.Status != PriceSuggestionStatus.Pending)
            {
                return false;
            }

            var mappedSuggestion = _mapper.Map<PriceSuggestion>(existingSuggestion);
            mappedSuggestion.Status = (PriceSuggestionStatus)updatedSuggestionStatus;

            var success = await _priceSuggestionRepository.UpdatePriceSuggestionStatus(existingSuggestion, mappedSuggestion);

            if (!success)
            {
                return false;
            }

            return true;
        }
    }
}
