﻿using SimpleWebStore.DTO;
using SimpleWebStore.Requests;

namespace SimpleWebStore.Services.Interfaces
{
    public interface IValidationService
    {
        bool ValidateUser(UserDTO user);

        bool ValidateProduct(ProductDTO product);

        bool ValidatePriceSuggestion(PriceSuggestionDTO priceSuggestion);

        bool ValidateDeleteProductRequest(DeleteProductRequest deleteRequest);

        bool ValidateUpdatePriceSuggestionStatusRequest(UpdatePriceSuggestionStatusRequest updateRequest);

        Task<bool> ValidateSuggestionId(int id);

        Task<bool> ValidateUserId(int id);

        Task<bool> ValidateProductId(int id);
    }
}
