﻿using SimpleWebStore.DTO;
using SimpleWebStore.Services;

namespace SimpleWebStore.Services.Interfaces
{
    public interface IPriceSuggestionService
    {
        Task<PriceSuggestionDTO[]> GetPriceSuggestions();

        Task<PriceSuggestionDTO[]> GetPriceSuggestionsByUserId(int userId);

        Task<bool> CreatePriceSuggestion(PriceSuggestionDTO priceSuggestion);

        Task<bool> UpdatePriceSuggestionStatus(int updatedSuggestionId, int updatedSuggestionStatus);
    }
}
