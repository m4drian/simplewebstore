﻿using SimpleWebStore.DTO;
using SimpleWebStore.Enums;
using SimpleWebStore.Models;
using SimpleWebStore.Services;

namespace SimpleWebStore.Services.Interfaces
{
    public interface IUserService
    {
        Task<bool> RegisterUser(UserDTO user);

        Task<bool> CanUserGetAccess(int id, UserRole role);
    }
}
