﻿using Microsoft.AspNetCore.Mvc;
using SimpleWebStore.DTO;
using SimpleWebStore.Services;

namespace SimpleWebStore.Services.Interfaces
{
    public interface IProductService
    {
        Task<List<ProductDTO>> GetProducts();

        Task<ProductDTO> GetProductById(int id);

        Task<List<ProductDTO>> GetProductsByName([FromQuery] string name);

        Task<List<ProductDTO>> GetProductsByCategory(string category);

        Task<bool> CreateProduct(ProductDTO product);

        Task<bool> UpdateProduct(ProductDTO product);

        Task<bool> DeleteProductById(int id);
    }
}
