﻿using AutoMapper;
using SimpleWebStore.DTO;
using SimpleWebStore.Enums;
using SimpleWebStore.Models;
using SimpleWebStore.Repositories.Interfaces;
using SimpleWebStore.Services.Interfaces;

namespace SimpleWebStore.Services
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;
        private readonly IMapper _mapper;

        public UserService(IUserRepository userRepository, IMapper mapper)
        {
            _userRepository = userRepository;
            _mapper = mapper;
        }

        public async Task<bool> CanUserGetAccess(int id, UserRole role)
        {
            var userToCheck = await _userRepository.GetUserById(id);

            if (userToCheck.Role == role) { return true; }

            return false;
        }

        public async Task<bool> RegisterUser(UserDTO user)
        {
            var mappedUser = _mapper.Map<User>(user);
            user.Role = (UserRole)user.Role;
            return await _userRepository.CreateUser(mappedUser);
        }
    }
}
