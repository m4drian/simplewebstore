﻿using AutoMapper;
using SimpleWebStore.DTO;
using SimpleWebStore.Enums;
using SimpleWebStore.Repositories.Interfaces;
using SimpleWebStore.Requests;
using SimpleWebStore.Services.Interfaces;

namespace SimpleWebStore.Services
{
    public class ValidationService : IValidationService
    {
        private readonly IPriceSuggestionRepository _priceSuggestionRepository;
        private readonly IProductRepository _productRepository;
        private readonly IUserRepository _userRepository;

        public ValidationService(IUserRepository userRepository, IPriceSuggestionRepository priceSuggestionRepository,IProductRepository productRepository)
        {
            _priceSuggestionRepository = priceSuggestionRepository;
            _productRepository = productRepository;
            _userRepository = userRepository;
        }

        public bool ValidateUser(UserDTO user)
        {
            if (string.IsNullOrWhiteSpace(user.Name))
            {
                return false;
            }

            if (user.Role != UserRole.Employee && user.Role != UserRole.Customer)
            {
                return false;
            }

            if (string.IsNullOrWhiteSpace(user.Login))
            {
                return false;
            }

            if (string.IsNullOrWhiteSpace(user.PasswordHash))
            {
                return false;
            }

            return true;
        }

        public bool ValidateProduct(ProductDTO product)
        {
            if (string.IsNullOrWhiteSpace(product.Name))
            {
                return false;
            }

            if (string.IsNullOrWhiteSpace(product.Category))
            {
                return false;
            }

            if (product.Price <= 0)
            {
                return false;
            }

            return true;
        }

        public bool ValidatePriceSuggestion(PriceSuggestionDTO priceSuggestion)
        {
            if (priceSuggestion.NewPrice <= 0)
            {
                return false;
            }

            if (priceSuggestion.UserId <= 0)
            {
                return false;
            }

            if (priceSuggestion.ProductId <= 0)
            {
                return false;
            }

            return true;
        }

        public async Task<bool> ValidateUserId(int id)
        {
            if (id <= 0)
                return false;

            var user = await _userRepository.GetUserById(id);

            if (user == null)
            {
                return false;
            }

            return true;

        }

        public async Task<bool> ValidateSuggestionId(int id)
        {
            if (id <= 0)
                return false;

            var suggestion = await _priceSuggestionRepository.GetPriceSuggestionById(id);

            if (suggestion == null)
            {
                return false;
            }

            return true;
        }

        public async Task<bool> ValidateProductId(int id)
        {
            if (id <= 0)
                return false;

            var product = await _productRepository.GetProductById(id);

            if (product == null)
            {
                return false;
            }

            return true;
        }

        public bool ValidateDeleteProductRequest(DeleteProductRequest deleteRequest)
        {
            if (deleteRequest == null || deleteRequest.UserId <= 0 || deleteRequest.ProductId <= 0)
            {
                return false;
            }

            return true;
        }

        public bool ValidateUpdatePriceSuggestionStatusRequest(UpdatePriceSuggestionStatusRequest updateRequest)
        {
            if (updateRequest == null ||
                updateRequest.UserId <= 0 ||
                updateRequest.PriceSuggestionId <= 0 ||
                !Enum.IsDefined(typeof(PriceSuggestionStatus), updateRequest.PriceSuggestionStatus))
            {
                return false;
            }

            return true;
        }
    }
}
