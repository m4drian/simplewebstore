﻿using AutoMapper;
using SimpleWebStore.DTO;
using SimpleWebStore.Models;
using SimpleWebStore.Repositories.Interfaces;
using SimpleWebStore.Services.Interfaces;

namespace SimpleWebStore.Services
{
    public class ProductService : IProductService
    {
        private readonly IProductRepository _productRepository;
        private readonly IMapper _mapper;

        public ProductService(IProductRepository repository, IMapper mapper)
        {
            _productRepository = repository;
            _mapper = mapper;
        }

        public async Task<List<ProductDTO>> GetProducts()
        {
            var products = await _productRepository.GetProducts();
            var mappedProducts = _mapper.Map<List<ProductDTO>>(products);
            return mappedProducts;
        }

        public async Task<ProductDTO> GetProductById(int id)
        {
            var product = await _productRepository.GetProductById(id);
            var mappedProduct = _mapper.Map<ProductDTO>(product);
            return mappedProduct;
        }

        public async Task<List<ProductDTO>> GetProductsByName(string name)
        {
            var products = await _productRepository.GetProductsByProductname(name);
            var mappedProducts = _mapper.Map<List<ProductDTO>>(products);
            return mappedProducts;
        }

        public async Task<List<ProductDTO>> GetProductsByCategory(string category)
        {
            var products = await _productRepository.GetProductsByCategory(category);
            var mappedProducts = _mapper.Map<List<ProductDTO>>(products);
            return mappedProducts;
        }

        public async Task<bool> CreateProduct(ProductDTO product)
        {
            var mappedProduct = _mapper.Map<Product>(product);
            return await _productRepository.CreateProduct(mappedProduct);
        }

        public async Task<bool> UpdateProduct(ProductDTO product)
        {
            var mappedProduct = _mapper.Map<Product>(product);
            return await _productRepository.UpdateProduct(mappedProduct);
        }

        public async Task<bool> DeleteProductById(int id)
        {
            return await _productRepository.DeleteProductById(id);
        }
    }
}
