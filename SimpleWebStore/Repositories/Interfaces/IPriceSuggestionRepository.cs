﻿using SimpleWebStore.Models;

namespace SimpleWebStore.Repositories.Interfaces
{
    public interface IPriceSuggestionRepository
    {
        Task<PriceSuggestion> GetPriceSuggestionById(int id);

        Task<PriceSuggestion[]> GetPriceSuggestions();

        Task<PriceSuggestion[]> GetPriceSuggestionsByProductId(int productId);

        Task<PriceSuggestion[]> GetPriceSuggestionsByUserId(int userId);

        Task<int> GetAmountOfPriceSuggestionsByProductIdAndUserId(int productId, int userId);

        Task<bool> CreatePriceSuggestion(PriceSuggestion priceSuggestion);

        Task<bool> UpdatePriceSuggestion(PriceSuggestion priceSuggestion);

        Task<bool> UpdatePriceSuggestionStatus(PriceSuggestion existingSuggestion, PriceSuggestion updatedSuggestion);

        Task<bool> DeletePriceSuggestionById(int id);

        Task<bool> Save();


    }
}
