﻿using SimpleWebStore.Models;

namespace SimpleWebStore.Repositories.Interfaces
{
    public interface IUserRepository
    {
        Task<bool> UserExists(string login);

        Task<User> GetUserById(int id);

        Task<User> GetUserByLogin(string login);

        Task<User[]> GetUsersByName(string name);

        Task<User[]> GetUsers();

        Task<bool> CreateUser(User user);

        Task<bool> UpdateUser(User user);

        Task<bool> DeleteUserById(int id);

        Task<bool> Save();
    }
}
