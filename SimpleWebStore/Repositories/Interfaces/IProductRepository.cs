﻿using SimpleWebStore.Models;

namespace SimpleWebStore.Repositories.Interfaces
{
    public interface IProductRepository
    {
        Task<bool> ProductExists(int id);

        Task<Product> GetProductById(int id);

        Task<IEnumerable<Product>> GetProducts();

        Task<IEnumerable<Product>> GetProductsByProductname(string productname);

        Task<IEnumerable<Product>> GetProductsByCategory(string category);

        Task<bool> CreateProduct(Product product);

        Task<bool> UpdateProduct(Product product);

        Task<bool> DeleteProductById(int id);

        Task<bool> Save();
    }
}
