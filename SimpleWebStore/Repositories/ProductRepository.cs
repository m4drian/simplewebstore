﻿using Microsoft.EntityFrameworkCore;
using SimpleWebStore.Data;
using SimpleWebStore.Models;
using SimpleWebStore.Repositories.Interfaces;
using System.Collections.Immutable;

namespace SimpleWebStore.Repositories
{
    public class ProductRepository : IProductRepository
    {
        private readonly DataContext _context;

        public ProductRepository(DataContext context)
        {
            _context = context;
        }

        public async Task<bool> ProductExists(int id)
        {
            return await _context.Products.AnyAsync(p => p.Id == id);
        }

        public async Task<Product> GetProductById(int id)
        {
            return await _context.Products.FindAsync(id);
        }

        public async Task<IEnumerable<Product>> GetProductsByCategory(string category)
        {
            return await _context.Products.Where(p => p.Category == category).OrderBy(p => p.Id).ToArrayAsync();
        }

        public async Task<IEnumerable<Product>> GetProductsByProductname(string productname)
        {
            return await _context.Products.Where(p => p.Name.Contains(productname)).OrderBy(p => p.Id).ToListAsync();
        }

        public async Task<IEnumerable<Product>> GetProducts()
        {
            return await _context.Products.ToListAsync();
        }

        public async Task<bool> CreateProduct(Product product)
        {
            try
            {
                await _context.Products.AddAsync(product);
                await _context.SaveChangesAsync();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public async Task<bool> UpdateProduct(Product product)
        {
            try
            {
                _context.Products.Update(product);
                await _context.SaveChangesAsync();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public async Task<bool> DeleteProductById(int id)
        {
            try
            {
                var productToDelete = await _context.Products.FindAsync(id);
                if (productToDelete == null)
                    return false;

                var priceSuggestionsToDelete = _context.PriceSuggestions
                    .Where(ps => ps.ProductId == id)
                    .ToList();

                foreach (var priceSuggestion in priceSuggestionsToDelete)
                {
                    _context.PriceSuggestions.Remove(priceSuggestion);
                }

                _context.Products.Remove(productToDelete);
                await _context.SaveChangesAsync();
                return true;
            }
            catch (Exception)
            {
                // Handle any exceptions that may occur during the delete process
                return false;
            }
        }

        public async Task<bool> Save()
        {
            return await _context.SaveChangesAsync() > 0;
        }
    }
}
