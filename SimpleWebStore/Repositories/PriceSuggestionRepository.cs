﻿using Microsoft.CodeAnalysis;
using Microsoft.EntityFrameworkCore;
using SimpleWebStore.Data;
using SimpleWebStore.Models;
using SimpleWebStore.Repositories.Interfaces;

namespace SimpleWebStore.Repositories
{
    public class PriceSuggestionRepository : IPriceSuggestionRepository
    {
        private readonly DataContext _context;
        public PriceSuggestionRepository(DataContext context)
        {
            _context = context;
        }

        public async Task<PriceSuggestion> GetPriceSuggestionById(int id)
        {
            return await _context.PriceSuggestions.FindAsync(id);
        }

        public async Task<PriceSuggestion[]> GetPriceSuggestions()
        {
            return await _context.PriceSuggestions
                .OrderBy(ps => ps.Id)
                .ToArrayAsync();
        }

        public async Task<PriceSuggestion[]> GetPriceSuggestionsByProductId(int productId)
        {
            return await _context.PriceSuggestions
                .Where(ps => ps.ProductId == productId)
                .OrderBy(ps => ps.Id)
                .ToArrayAsync();
        }

        public async Task<PriceSuggestion[]> GetPriceSuggestionsByUserId(int userId)
        {
            return await _context.PriceSuggestions
                .Where(ps => ps.UserId == userId)
                .OrderBy(ps => ps.Id)
                .ToArrayAsync();
        }

        public async Task<int> GetAmountOfPriceSuggestionsByProductIdAndUserId(int productId, int userId)
        {
            return await _context.PriceSuggestions
                .Where(ps => ps.ProductId == productId && ps.UserId == userId)
                .CountAsync();
        }

        public async Task<bool> CreatePriceSuggestion(PriceSuggestion priceSuggestion)
        {
            try
            {
                await _context.PriceSuggestions.AddAsync(priceSuggestion);
                await _context.SaveChangesAsync();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public async Task<bool> UpdatePriceSuggestion(PriceSuggestion priceSuggestion)
        {
            try
            {
                _context.PriceSuggestions.Update(priceSuggestion);
                await _context.SaveChangesAsync();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public async Task<bool> UpdatePriceSuggestionStatus(PriceSuggestion existingSuggestion, PriceSuggestion updatedSuggestion)
        {
            try
            {
                existingSuggestion.Status = updatedSuggestion.Status;
                await _context.SaveChangesAsync();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public async Task<bool> DeletePriceSuggestionById(int id)
        {
            try
            {
                var priceSuggestionToDelete = await _context.PriceSuggestions.FindAsync(id);
                if (priceSuggestionToDelete == null)
                    return false;

                _context.PriceSuggestions.Remove(priceSuggestionToDelete);
                await _context.SaveChangesAsync();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public async Task<bool> Save()
        {
            return await _context.SaveChangesAsync() > 0;
        }
    }
}
