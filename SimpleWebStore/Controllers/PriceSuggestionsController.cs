﻿using Azure.Core;
using Microsoft.AspNetCore.Http.HttpResults;
using Microsoft.AspNetCore.Mvc;
using SimpleWebStore.DTO;
using SimpleWebStore.Enums;
using SimpleWebStore.Models;
using SimpleWebStore.Requests;
using SimpleWebStore.Services;
using SimpleWebStore.Services.Interfaces;

/*
endpoints:

    POST /api/pricesuggestions (Only Customer): Submit a new price suggestion.
    PUT /api/pricesuggestions/{id} (Only Employee): Accept or reject a price suggestion.
    GET /api/pricesuggestions/{product_id}: Retrieve price suggestions based on user id.
    GET /api/pricesuggestions (Only Employee): Retrieve all price suggestions.
*/

namespace SimpleWebStore.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PriceSuggestionsController : ControllerBase
    {

        private readonly IPriceSuggestionService _priceSuggestionService;
        private readonly IUserService _userService;
        private readonly IValidationService _validationService;

        public PriceSuggestionsController(IPriceSuggestionService priceSuggestionService, IUserService userService, IValidationService validationService)
        {
            _priceSuggestionService = priceSuggestionService;
            _userService = userService;
            _validationService = validationService;

        }

        // GET: api/PriceSuggestions
        [HttpGet]
        [ProducesResponseType(200, Type = typeof(IEnumerable<Product>))]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(403)]
        [ProducesResponseType(500)]
        public async Task<ActionResult<IEnumerable<PriceSuggestionDTO>>> GetPriceSuggestions([FromQuery] int requestUserId)
        {
            if (!await _validationService.ValidateUserId(requestUserId))
            {
                return BadRequest("Invalid user identity.");
            }

            if (!await _userService.CanUserGetAccess(requestUserId, UserRole.Employee)) 
            {
                return StatusCode(StatusCodes.Status403Forbidden, "This user doesn't have the required role to access this resource");
            }

            try
            {
                var priceSuggestions =  await _priceSuggestionService.GetPriceSuggestions();

                if (priceSuggestions == null)
                {
                    return StatusCode(StatusCodes.Status400BadRequest, "No price suggestions found.");
                }
                return Ok(priceSuggestions.ToList());
            }
            catch (Exception)
            {
                return StatusCode(500, "Internal Server Error when trying to get price suggestions.");
            }
        }

        // GET: api/PriceSuggestions/5
        [HttpGet("{userId}")]
        [ProducesResponseType(200, Type = typeof(IEnumerable<Product>))]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(403)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public async Task<ActionResult<IEnumerable<PriceSuggestionDTO>>> GetPriceSuggestionsByUserId(int userId)
        {
            if (!await _validationService.ValidateUserId(userId))
            {
                return BadRequest("Invalid user identity.");
            }

            try
            {
                var priceSuggestions = await _priceSuggestionService.GetPriceSuggestionsByUserId(userId);

                if (priceSuggestions == null)
                {
                    return StatusCode(StatusCodes.Status404NotFound, "No price suggestions found");
                }

                return Ok(priceSuggestions);
            }
            catch (Exception)
            {
                return StatusCode(500, "Internal Server Error when trying to get price suggestions.");
            }
        }

        // POST: api/pricesuggestions (Only Customer)
        [HttpPost]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(403)]
        public async Task<IActionResult> CreatePriceSuggestion(CreatePriceSuggestionRequest request)
        {
            if (!await _validationService.ValidateUserId(request.UserId))
            {
                return BadRequest("Invalid user identity.");
            }

            if (!await _userService.CanUserGetAccess(request.UserId, UserRole.Customer))
            {
                return StatusCode(403, "This user doesn't have the required role to access this resource");
            }

            if (!_validationService.ValidatePriceSuggestion(request.PriceSuggestion))
            {
                return BadRequest("Invalid user identity.");
            }

            try
            {
                bool isCreated = await _priceSuggestionService.CreatePriceSuggestion(request.PriceSuggestion);

                if (!isCreated)
                {
                    return StatusCode(400, "Failed to create a price suggestion.");
                }

                return StatusCode(StatusCodes.Status201Created, "Successfully created a price suggestion.");
            }
            catch(Exception)
            {
                return StatusCode(500, "Internal Server Error when trying to create a price suggestion");
            }
        }

        // PUT /api/pricesuggestions
        [HttpPut]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        [ProducesResponseType(403)]
        public async Task<IActionResult> UpdatePriceSuggestionStatus(UpdatePriceSuggestionStatusRequest request)
        {
            if (!await _userService.CanUserGetAccess(request.UserId, UserRole.Employee))
            {
                return StatusCode(403, "This user doesn't have the required role to access this resource");
            }

            if (_validationService.ValidateUpdatePriceSuggestionStatusRequest(request))
            {
                return BadRequest("Invalid input data.");
            }

            try 
            {
                bool isUpdated = await _priceSuggestionService.UpdatePriceSuggestionStatus(request.PriceSuggestionId, request.PriceSuggestionStatus);

                if (!isUpdated)
                {
                    return StatusCode(400, "Failed to update a price suggestion.");
                }

                return Ok("Successfully update a price suggestion.");
            }
            catch (Exception)
            {
                return StatusCode(500, "Internal Server Error when trying to update a price suggestion");
            }
        }

    }
}
