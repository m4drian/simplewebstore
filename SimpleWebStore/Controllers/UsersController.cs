﻿
using Microsoft.AspNetCore.Mvc;
using SimpleWebStore.DTO;
using SimpleWebStore.Services;
using SimpleWebStore.Services.Interfaces;

/*
endpoints:

    POST /api/user/register: Register a new user.
*/

namespace SimpleWebStore.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly IUserService _userService;
        private readonly IValidationService _validationService;

        public UsersController(IUserService userService, IValidationService validationService)
        {
            _userService = userService;
            _validationService = validationService;
        }

        [HttpPost("register")]
        [ProducesResponseType(200)] 
        [ProducesResponseType(400)]
        public async Task<IActionResult> RegisterUser([FromBody] UserDTO user)
        {
            if (!_validationService.ValidateUser(user))
            {
                return BadRequest("Invalid user data");
            }

            try
            {
                bool isSuccess = await _userService.RegisterUser(user);

                if (!isSuccess)
                {
                    return StatusCode(400, "Failed to register a new user.");
                }

                return Ok("Successfully registered user.");
            }
            catch (Exception)
            {
                return StatusCode(500, "Internal server error when trying to add a new user.");
            }
        }
    }
}
