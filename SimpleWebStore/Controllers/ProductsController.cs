﻿using Azure.Core;
using Microsoft.AspNetCore.Http.HttpResults;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using SimpleWebStore.DTO;
using SimpleWebStore.Enums;
using SimpleWebStore.Models;
using SimpleWebStore.Requests;
using SimpleWebStore.Services;
using SimpleWebStore.Services.Interfaces;

/*
endpoints:

    GET /api/products/{id}: Retrieve product by id.
    GET /api/products/name/{name}: Retrieve products by name.
    GET /api/products/category/{category}: Retrieve products by category.
    POST /api/products (Only Employee): Add a new product.
    PUT /api/products/{id} (Only Employee): Update product details.
    DELETE /api/products/{id} (Only Employee): Remove a product.
*/

namespace SimpleWebStore.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductsController : ControllerBase
    {
        private readonly IProductService _productService;
        private readonly IUserService _userService;
        private readonly IValidationService _validationService;

        public ProductsController(IProductService productService, IUserService userService, IValidationService validationService)
        {
            _productService = productService;
            _userService = userService;
            _validationService = validationService;

        }

        [HttpGet("{productId}")]
        [ProducesResponseType(200, Type = typeof(ProductDTO))]
        public async Task<ActionResult<ProductDTO>> GetProductsById(int productId)
        {
            if(!await _validationService.ValidateProductId(productId))
            {
                return BadRequest("Invalid input data.");
            }

            try
            {
                ProductDTO product = await _productService.GetProductById(productId);

                if (product == null)
                {
                    return StatusCode(StatusCodes.Status404NotFound, "Product not found.");
                }

                return Ok(product);
            }
            catch (Exception)
            {
                return StatusCode(500, "An internal error occurred while fetching the product");
            }

        }

        [HttpGet("name/{name}")]
        [ProducesResponseType(200, Type = typeof(List<ProductDTO>))]
        public async Task<ActionResult<List<ProductDTO>>> GetProductsByName(string name)
        {
            if(name.IsNullOrEmpty())
            {
                return BadRequest("Invalid input data.");
            }

            try
            {
                var productList = await _productService.GetProductsByName(name);

                if (productList == null || productList.Count <= 0)
                {
                    return StatusCode(StatusCodes.Status404NotFound, "Product not found.");
                }

                return Ok(productList);
            }
            catch (Exception)
            {
                return StatusCode(500, "An internal error occurred when fetching products");
            }
        }

        [HttpGet("category/{category}")]
        [ProducesResponseType(200, Type = typeof(List<ProductDTO>))]
        public async Task<ActionResult<List<ProductDTO>>> GetProductsByCategory(string category)
        {
            if (category.IsNullOrEmpty())
            {
                return BadRequest("Invalid input data.");
            }

            try
            {
                var productList = await _productService.GetProductsByCategory(category);

                if (productList == null || productList.Count <= 0)
                {
                    return StatusCode(StatusCodes.Status404NotFound, "Category of products not found.");
                }

                return Ok(productList);
            }
            catch (Exception)
            {
                return StatusCode(500, "An internal error occurred when fetching products");
            }
        }

        // POST: api/Products
        [HttpPost]
        [ProducesResponseType(201, Type = typeof(Product))]
        [ProducesResponseType(400)]
        public async Task<IActionResult> CreateProduct([FromBody] CreateProductRequest request)
        {
            if (!await _validationService.ValidateUserId(request.UserId))
            {
                return BadRequest("Invalid user identity.");
            }

            if (!await _userService.CanUserGetAccess(request.UserId, UserRole.Employee))
            {
                return StatusCode(403, "This user doesn't have the required role to access this resource");
            }

            if (!_validationService.ValidateProduct(request.Product))
            {
                return BadRequest("Invalid input data.");
            }

            try
            {
                bool isCreated = await _productService.CreateProduct(request.Product);

                if (!isCreated)
                {
                    return StatusCode(400, "Failed to create product.");
                }

                return StatusCode(201, "Product created");
            }
            catch(Exception)
            {
                return StatusCode(500, "An internal error occurred when creating product");
            }
        }

        // PUT: api/Products/5
        [HttpPut]
        [ProducesResponseType(204)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> UpdateProduct([FromBody] UpdateProductRequest request)
        {
            if (!await _validationService.ValidateProductId(request.Product.Id))
            {
                return BadRequest("Invalid product identity.");
            }

            if (!await _validationService.ValidateUserId(request.UserId))
            {
                return BadRequest("Invalid user identity.");
            }

            if (!await _userService.CanUserGetAccess(request.UserId, UserRole.Employee))
            {
                return StatusCode(403, "This user doesn't have permission to access this resource");
            }

            if (!_validationService.ValidateProduct(request.Product))
            {
                return BadRequest("Invalid input data.");
            }

            try
            {
                var isUpdated = await _productService.UpdateProduct(request.Product);

                if (!isUpdated)
                {
                    return StatusCode(StatusCodes.Status404NotFound, "Failed to update product");
                }

                return StatusCode(StatusCodes.Status204NoContent, "Successfully updated product");
            }
            catch (Exception)
            {
                return StatusCode(500, "An internal error occurred when updating product");
            }
        }

        // DELETE: api/Products/5
        [HttpDelete]
        [ProducesResponseType(204)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> DeleteProduct([FromBody] DeleteProductRequest request)
        {
            if (!await _userService.CanUserGetAccess(request.UserId, UserRole.Employee))
            {
                return StatusCode(403, "This user doesn't have permission to access this resource");
            }

            if (!_validationService.ValidateDeleteProductRequest(request))
            {
                return BadRequest("Invalid input data.");
            }

            try
            {
                var isDeleted = await _productService.DeleteProductById(request.ProductId);

                if (!isDeleted)
                {
                    return StatusCode(404, "Failed to delete product");
                }

                return StatusCode(StatusCodes.Status204NoContent, "Successfully deleted product");
            }
            catch (Exception)
            {
                return StatusCode(500, "An internal error occurred when updating product");
            }
        }

    }
}
