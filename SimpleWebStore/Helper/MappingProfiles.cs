﻿using AutoMapper;
using SimpleWebStore.DTO;
using SimpleWebStore.Models;

namespace SimpleWebStore.Helper
{
    public class MappingProfiles : Profile
    {
        public MappingProfiles()
        {
            CreateMap<PriceSuggestion, PriceSuggestionDTO>();
            CreateMap<PriceSuggestionDTO, PriceSuggestion>();

            CreateMap<ProductDTO, Product>();
            CreateMap<Product, ProductDTO>();

            CreateMap<UserDTO, User>();
            CreateMap<User, UserDTO>();
        }

    }
}
