﻿using SimpleWebStore.DTO;

namespace SimpleWebStore.Requests
{
    public class CreatePriceSuggestionRequest
    {
            public int UserId { get; set; }

            public PriceSuggestionDTO PriceSuggestion { get; set; }
    }
}
