﻿namespace SimpleWebStore.Requests
{
    public class DeleteProductRequest
    {
        public int UserId { get; set; }

        public int ProductId { get; set; }
    }
}
