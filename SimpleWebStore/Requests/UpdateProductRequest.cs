﻿using Microsoft.AspNetCore.Mvc;
using SimpleWebStore.DTO;

namespace SimpleWebStore.Requests
{
    public class UpdateProductRequest
    {
        public int UserId { get; set; }

        public ProductDTO Product { get; set; }
    }
}
