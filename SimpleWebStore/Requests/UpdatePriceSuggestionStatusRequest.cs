﻿using Microsoft.AspNetCore.Mvc;

namespace SimpleWebStore.Requests
{
    public class UpdatePriceSuggestionStatusRequest
    {
        public int UserId { get; set; }

        public int PriceSuggestionId { get; set; }

        public int PriceSuggestionStatus { get; set; }
    }
}
