﻿using SimpleWebStore.DTO;

namespace SimpleWebStore.Requests
{
    public class CreateProductRequest
    {
        public int UserId { get; set; }

        public ProductDTO Product { get; set; }
    }
}
