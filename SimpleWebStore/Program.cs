using Microsoft.EntityFrameworkCore;
using SimpleWebStore;
using SimpleWebStore.Data;
using SimpleWebStore.Repositories;
using SimpleWebStore.Repositories.Interfaces;
using SimpleWebStore.Services;
using SimpleWebStore.Services.Interfaces;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddControllers();
builder.Services.AddTransient<SeedData>();
builder.Services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());
builder.Services.AddScoped<IPriceSuggestionRepository, PriceSuggestionRepository>();
builder.Services.AddScoped<IProductRepository, ProductRepository>();
builder.Services.AddScoped<IUserRepository, UserRepository>();
builder.Services.AddScoped<IPriceSuggestionService, PriceSuggestionService>();
builder.Services.AddScoped<IProductService, ProductService>();
builder.Services.AddScoped<IUserService, UserService>();
builder.Services.AddScoped<IValidationService, ValidationService>();
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddDbContext<DataContext>(options =>
{
    options.UseNpgsql(builder.Configuration.GetConnectionString("DefaultConnection"));
});

var app = builder.Build();

if (args.Length == 1 && args[0].ToLower() == "seeddata")
    SeedWithData(app);

static void SeedWithData(IHost app)
{
    var scopedFactory = app.Services.GetService<IServiceScopeFactory>();

    if (scopedFactory == null)
    {
        Console.WriteLine("ServiceScopeFactory is null. Exiting the SeedData method.");
        return;
    }

    using var scope = scopedFactory.CreateScope();
    var service = scope.ServiceProvider.GetService<SimpleWebStore.SeedData>();

    service?.SeedDataContext();

}

if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
