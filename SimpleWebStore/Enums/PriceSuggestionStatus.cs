﻿namespace SimpleWebStore.Enums
{
    public enum PriceSuggestionStatus
    {
        Pending,
        Accepted,
        Rejected
    }
}
