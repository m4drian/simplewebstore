﻿namespace SimpleWebStore.Enums
{
    public enum UserRole
    {
        Customer,
        Employee
    }
}
