﻿using SimpleWebStore.Data;
using SimpleWebStore.Enums;
using SimpleWebStore.Models;

namespace SimpleWebStore
{
    public class SeedData
    {
        private readonly DataContext _dataContext;
        public SeedData(DataContext context)
        {
            this._dataContext = context;
        }
        public void SeedDataContext()
        {
            var userCustomer = new User
            {
                Name = "Customer",
                Role = UserRole.Customer,
                Login = "Customer",
                PasswordHash = "Customer"
            };

            var userEmployee = new User
            {
                Name = "Employee",
                Role = UserRole.Employee,
                Login = "Employee",
                PasswordHash = "Employee"
            };

            var products = new List<Product>
            {
                new Product
                {
                    Name = "Product1",
                    Description = "Cool product",
                    Category = "Electronics",
                    Price = (decimal)12.2
                },
                new Product
                {
                    Name = "Product2",
                    Description = "Also a cool product",
                    Category = "Books",
                    Price = (decimal)10.2
                }
            };

            var priceSuggestions = new List<PriceSuggestion>
            {
                new PriceSuggestion {
                    NewPrice = (decimal)15.99,
                    Status = PriceSuggestionStatus.Pending,
                    User = userCustomer,
                    Product = products.First()
                },
                new PriceSuggestion {
                    NewPrice = (decimal)16,
                    Status = PriceSuggestionStatus.Pending,
                    User = userCustomer,
                    Product = products.First()
                }
            };

            _dataContext.Users.Add(userCustomer);
            _dataContext.Users.Add(userEmployee);
            _dataContext.Products.AddRange(products);
            _dataContext.PriceSuggestions.AddRange(priceSuggestions);
            _dataContext.SaveChanges();

        }
    }
}
