﻿using Moq;
using SimpleWebStore.DTO;
using SimpleWebStore.Enums;
using SimpleWebStore.Models;
using SimpleWebStore.Repositories.Interfaces;
using SimpleWebStore.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleWebStore.Test.Service
{
    public class ValidationServiceTests
    {
        private readonly Mock<IUserRepository> _userRepositoryMock = new();
        private readonly Mock<IPriceSuggestionRepository> _priceSuggestionRepositoryMock = new();
        private readonly Mock<IProductRepository> _productRepositoryMock = new();

        [Fact]
        public void ValidateUser_ShouldReturnTrue_WhenUserIsValid()
        {
            var user = new UserDTO
            {
                Name = "Test",
                Role = UserRole.Customer,
                Login = "test@test.com",
                PasswordHash = "test"
            };

            var validationService = new ValidationService(_userRepositoryMock.Object, _priceSuggestionRepositoryMock.Object, _productRepositoryMock.Object);

            bool result = validationService.ValidateUser(user);

            Assert.True(result);
        }

        [Fact]
        public void ValidateUser_ShouldReturnFalse_WhenUserNameIsNullOrWhiteSpace()
        {
            var user = new UserDTO
            {
                Name = "",
                Role = UserRole.Customer,
                Login = "test@test.com",
                PasswordHash = "test"
            };

            var validationService = new ValidationService(_userRepositoryMock.Object, _priceSuggestionRepositoryMock.Object, _productRepositoryMock.Object);

            bool result = validationService.ValidateUser(user);

            Assert.False(result);
        }

        [Fact]
        public async Task ValidateUserId_ShouldReturnTrue_WhenUserExists()
        {
            int userId = 1;
            var user = new User { Id = userId };
            _userRepositoryMock.Setup(repo => repo.GetUserById(userId)).ReturnsAsync(user);

            var validationService = new ValidationService(_userRepositoryMock.Object, _priceSuggestionRepositoryMock.Object, _productRepositoryMock.Object);

            bool result = await validationService.ValidateUserId(userId);

            Assert.True(result);
        }

        [Fact]
        public async Task ValidateUserId_ShouldReturnFalse_WhenUserDoesntExist()
        {
            int userId = 1;
            var user = new User { Id = userId };
            int notUserId = 2;
            _userRepositoryMock.Setup(repo => repo.GetUserById(userId)).ReturnsAsync(user);

            var validationService = new ValidationService(_userRepositoryMock.Object, _priceSuggestionRepositoryMock.Object, _productRepositoryMock.Object);

            // Act
            bool result = await validationService.ValidateUserId(notUserId);

            // Assert
            Assert.False(result);
        }
    }
}
