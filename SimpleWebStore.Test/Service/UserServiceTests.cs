﻿using System.Threading.Tasks;
using Xunit;
using Moq;
using AutoMapper;
using SimpleWebStore.DTO;
using SimpleWebStore.Models;
using SimpleWebStore.Services;
using SimpleWebStore.Repositories.Interfaces;
using SimpleWebStore.Enums;

namespace SimpleWebStore.Test.Service
{
    public class UserServiceTests
    {
        private readonly Mock<IUserRepository> _userRepositoryMock = new();
        private readonly Mock<IMapper> _mapperMock = new();

        [Fact]
        public async Task CanUserGetAccess_ShouldReturnTrue_WhenRolesMatch()
        {
            int userId = 1;
            UserRole role = UserRole.Customer;
            var user = new User { Id = userId, Role = role };
            _userRepositoryMock.Setup(repo => repo.GetUserById(userId)).ReturnsAsync(user);

            var userService = new UserService(_userRepositoryMock.Object, _mapperMock.Object);

            bool result = await userService.CanUserGetAccess(userId, role);

            Assert.True(result);
        }

        [Fact]
        public async Task CanUserGetAccess_ShouldReturnFalse_WhenRolesDoNotMatch()
        {
            int userId = 1;
            UserRole roleToCheck = UserRole.Customer;
            UserRole actualRole = UserRole.Employee;
            var user = new User { Id = userId, Role = actualRole };
            _userRepositoryMock.Setup(repo => repo.GetUserById(userId)).ReturnsAsync(user);

            var userService = new UserService(_userRepositoryMock.Object, _mapperMock.Object);

            bool result = await userService.CanUserGetAccess(userId, roleToCheck);

            Assert.False(result);
        }
    }
}