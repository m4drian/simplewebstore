# SimpleWebStore



## Name
SimpleWebStore Web API

## Description
Business Logic<br />

he application aims to facilitate price negotiations for a product <br />
between a customer and an employee of an online store. <br />
The customer can submit a price proposal, <br />
and the employee has the option to accept or reject the proposal. <br />
In case of rejection, the customer can try again (up to 3 times). <br />
If the proposed price exceeds twice the base price of the product, <br />
the proposal is automatically rejected.

## Endpoints

POST /api/pricesuggestions (Only Customer): Submit a new price suggestion.<br />
PUT /api/pricesuggestions/{id} (Only Employee): Accept or reject a price suggestion.<br />
GET /api/pricesuggestions/{product_id}: Retrieve price suggestions based on user id.<br />
GET /api/pricesuggestions (Only Employee): Retrieve all price suggestions.<br />

GET /api/products/{id}: Retrieve product by id.<br />
GET /api/products/name/{name}: Retrieve products by name.<br />
GET /api/products/category/{category}: Retrieve products by category.<br />
POST /api/products (Only Employee): Add a new product.<br />
PUT /api/products/{id} (Only Employee): Update product details.<br />
DELETE /api/products/{id} (Only Employee): Remove a product.<br />

POST /api/user/register: Register a new user.<br />

## Tables
PriceSuggestion<br />
Product<br />
User

## Installation && Usage
All commands below need to be run from the command line in project folder

Project uses PostgeSQL database to store data

IMPORTANT!<br />
when connecting to a database connection string located in /SimpleWebStore/appsettings.json
needs to be changed

to build project:<br />
docker-compose up

to start migrations:<br />
dotnet ef database update InitialCreate

to populate database with example data(optional):<br />
dotnet run seeddata

then in order to test WebAPI swagger can be open in browser under:<br />
https://localhost:49145/swagger/

## Authors and acknowledgment
mepper

## License
MIT